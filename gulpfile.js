 var gulp = require('gulp');
 // var sass = require('gulp-sass');
 var babel = require('gulp-babel');
 var concat = require('gulp-concat');
 var minify = require('gulp-uglify');
 var rename = require('gulp-rename');
 var sourcemaps = require('gulp-sourcemaps');
 var cleanCSS = require('gulp-clean-css');
 var del = require('del');
 var paths = {
   styles: {
     src: [ 'src/css/*.css' ], // src: [ 'src/scss/*.scss' ],
     dest: 'dist/css/'
   },
   scripts: {
     src: [ 'src/js/polyfills/*.js', 'src/js/core/io.utilities.js', 'src/js/core/io.js', 'src/js/plugins/*.js', "!src/js/plugins/io.skeleton.js" ],
     dest: 'dist/js/'
   }
 };

 /* Not all tasks need to use streams, a gulpfile is just another node program
  * and you can use all packages available on npm, but it must return either a
  * Promise, a Stream or take a callback and call it
  */
 function clean() {
   // You can use multiple globbing patterns as you would with `gulp.src`,
   // for example if you are using del 2.0 or above, return its promise
   return del([ 'assets' ]);
 }

 /*
  * Define our tasks using plain functions
  */
 function styles() {
   return gulp.src(paths.styles.src)
     // .pipe(sass())
	  .pipe(concat('io.css'))
	  .pipe(gulp.dest(paths.styles.dest))
     .pipe(cleanCSS())
     .pipe(rename({ basename: 'io', suffix: '.min' }))
     .pipe(gulp.dest(paths.styles.dest));
 }

 function scripts() {
   return gulp.src(paths.scripts.src, { sourcemaps: true })
	  .pipe(sourcemaps.init())
	  .pipe(concat('io.js'))
	  .pipe(babel())
	  .pipe(gulp.dest(paths.scripts.dest))
     .pipe(minify())
	  .pipe(rename({ suffix: '.min' }))
	  .pipe(sourcemaps.write('.', { sourceRoot: paths.scripts.dest }))
     .pipe(gulp.dest(paths.scripts.dest));
 }

 function watch() {
   gulp.watch(paths.scripts.src, scripts);
   gulp.watch(paths.styles.src, styles);
 }

 /*
  * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
  */
 var build = gulp.series(clean, gulp.parallel(styles, scripts));

 /*
  * You can use CommonJS `exports` module notation to declare tasks
  */
 exports.clean = clean;
 exports.styles = styles;
 exports.scripts = scripts;
 exports.watch = watch;
 exports.build = build;

 /*
  * Define default task that can be called by just running `gulp` from cli
  */
 exports.default = build;
