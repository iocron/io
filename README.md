# IO Plugin Framework
Use some of the plugins provided (e.g. ionav) or create your own easy to use plugins based on a Pub/Sub System and MQL's (MediaQueryLists), see the src/js/plugins/io.skeleton.js file for more informations on creating your own io plugin.

This script is still in Alpha and will be later moved to Github.

## Use the Plugins provided:
Just include the /dist/io.min.js (required) and /dist/css/io.min.css (optional) into your project.

## Create your own io plugin:
See the src/js/plugins/io.skeleton.js file for more informations.
