class IoNavTouch extends Io {
	constructor(options={}){
		// Super Class Constructor (needed if you use inheritance)
		super(options);

		// Set Defaults, Options & Config
		this.name = "ionavTouch";
		this.defaults = {
			prefix: this.name + "-",
			mqlQuery: {						// Media Query Parameters (use a camelcase css notation)
				minWidth:"1025px",
				maxWidth:false,
			},
			elementsNav: [],				// Array (HTML Elements) | NodeList | String (Selector)
			linkSubmenuClickable: true, // Generates a additional submenu item on submenu's to click
			lang: document.querySelector("html[lang]") ? document.querySelector("html[lang]").lang.slice(0,2) : "en",
			langDict: {
				 "de": "Zur Kategorie ",
				 "en": "To category ",
				 "it": "Alla categoria ",
				 "es": "A la categoría ",
				 "fr": "À la catégorie "
			}
		};

		// Merge Plugin Defaults and User Options to one Config
		// (everything is accessible through this.config then)
		Object.assign(this.config, this.defaults, this.options);

		// Initialize Mobi Main-Logic
		this.sub("/init/", this.init);
		this.pub("/init/");
	}

	init(){
		this.mqlManager("/mqlMain/", this.config.mqlQuery, (mql) => {
			if(mql.matches){
				console.log("IS MATCHING");
			} else {
				console.log("IS NOT MATCHING");
			}
		});

		// Subscribers
		this.sub("/initElementsNav/", this.elementsNav);

		// Publishers & Events
		this.pub("/initElementsNav/", this.config.elementsNav);

		// Logging
		this.logAll();
	}

	elementsNav(selector){
		document.querySelectorAll(selector).forEach((el) => {
			el.querySelectorAll("ul li").forEach((li) => {
				let link = false;
				let subnav = false;

				li.childNodes.forEach((node) => {
					if(node.tagName == "A"){ link = node; }
					if(node.tagName == "A" || node.tagName == "UL"){ this.addClass(node, "-hasSubnav"); }
					if(node.tagName == "UL"){ subnav = node; }
				});

				if(link && subnav){
					this.addClass(li, "-hasSubnav");
					this.on(link, "click", (e) => {
						e.preventDefault();
						this.toggleClass(li, "-active");
						this.toggleClass(link, "-active");
						this.toggleClass(subnav, "-active");
					});

					// Add a additional link button (optional, see linkSubmenuClickable), so the user is able to click on the main category,
					// otherwise the user is only able to open the accordion/submenu, not the main category link itself
					if(this.config.linkSubmenuClickable && subnav){
						let linkSubmenuClickable = this.prependTo("li", subnav);
						this.addClass(linkSubmenuClickable, "-linkSubmenuClickable");
						linkSubmenuClickable.innerHTML = link.outerHTML;
						linkSubmenuClickable.querySelector("a").textContent = this.config.langDict[this.config.lang] + linkSubmenuClickable.querySelector("a").textContent;
					}
				}
			});
		});
	}
}
