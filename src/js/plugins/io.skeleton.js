/* THIS IS A IO PLUGIN SKELETON / TEMPLATE
	---------------------------------------
	Use it to create your own custom io plugins.

	How-To Use:
	1. First install the npm packages of this project: npm install
	2. Rename the class (IoSkeleton) and file name
	3. Near this.name rename it as well
	4. Prefill the Plugin Default Settings with whatever you need for your plugin
	5. Look at the init() class method comments (very down below) for how to use the io core and utilities
	6. Once you have something to test / developed, then use gulp to build it (type in "gulp" in your command line)
		(the entire io core + utilities + plugins will be bundled to /dist/js/io.js and /dist/js/io.min.js)
	7. Initialize your new plugin somewhere (see further below at the "USAGE" section)
		and have fun creating something awesome
*/

/* PROJECT STRUCTURE
	---------------------------------------
	The following JS Files will be bundled into /dist/js/io.js and /dist/js/io.min.js
		/src/js/core/		=> All IO Core Modules (do not change them!!)
		/src/js/polyfills/=>	All IO Polyfills (do not change them!!)
		/src/js/plugins/	=>	All IO Plugins

	The following CSS Files will be bundled into /dist/css/io.css and /dist/css/io.min.css
		/src/css/			=> All IO Styling (optional)
*/

/* USAGE / INITIALIZATION of a IO PLUGIN
	---------------------------------------
	let ioskeleton = new IoSkeleton({
		elements: <selectorOrElements>,
			media: {
				minWidth:"0px",
				maxWidth:"960px",
				...
		},
			...
	});
*/

// Rename this class + filename and use it as your own custom io plugin
class IoSkeleton extends Io {
	constructor(options={}){
		// Super Class Constructor (needed if you use inheritance)
		super(options);

		// Set Defaults, Options & Config
		this.name = "ioskeleton";		// Change this to your needs
		this.defaults = {					// All default params are optional, except "prefix"
			prefix: this.name + "-",	// Neccessary (change only if really needed)
			// mqlQuery: {					// Media Query Parameters (use a camelcase css notation)
			// 	minWidth:"0px",
			// 	maxWidth:"1024px"
			// },
			// elements: [],				// Array (HTML Elements) | NodeList | String
			// debug: false				// true | false (default) - Debug Messages
			// ...							// More user custom config variables
		};

		// Merge Plugin Defaults and User Options to one Config
		// (everything is accessible through this.config then)
		Object.assign(this.config, this.defaults, this.options);

		// Initialize Main Logic (this.init()) of your script as Pub/Sub Event
		this.sub("/init/", this.init);	// <= Will be published as /ioskeleton/init/
		this.pub("/init/");

		// Logging
		this.logAll();
	}

	// Create as many class methods you like
	// method1(){  }
	// method2(){  }

	init(){
		// BEST PRACTICE of creating IO PLUGINS
		// ---------------------------------------
		// Use this.ui for User Interface Elements of a plugin (if needed)
		// Use this.defaults as the default configuration of your plugin
		// Use this.config.elements as the default user input elements to process
		// Use this.logAll() if you need debugging information (only if this.config.debug is set to true)

		// SUBSCRIPTIONS and PUBLISHERS
		// ---------------------------------------
		// Regular Pub / Sub (usefull only inside / for this plugin/class)
		// this.sub("/classEvent/", (arg1, arg2) => { console.log("classEvent"); });
		// this.pub("/classEvent/"); // Or this.pub("/classEvent/", overrideArgument...);
		// this.pub("/classEvent/", arg1, arg2); // Publish a event with custom / overriding attributes

		// Global Pub / Sub (any pub/sub event can be accessed, even from other io plugins / classes)
		// io.sub("/globalEvent/", (arg1, arg2) => { console.log("globalEvent"); });
		// io.pub("/globalEvent/"); // Or this.io.pub("/globalEvent/", overrideArgument...);

		// Pub / Sub Inheritance (trigger multiple subscriptions by inheritance)
		// this.sub("/classEvent/", (arg1, arg2) => { console.log("classEvent"); });
		// this.sub("/classEvent/somethingElse/", (arg1, arg2) => { console.log("classEvent"); });
		// this.pub("/classEvent/"); // Will trigger the Subscriptions "/classEvent/" and "/classEvent/somethingElse/"

		// MQL MANAGER
		// ---------------------------------------
		// Setup a Mql Manager Subscription (sets a new mql query subscription)
		// (Use the defaults/options config to set the mqlQuery for the mqlManager (see the defaults setting)) or use a custom one
		// (You can choose any subscription path name you like (e.g. /mqlManager/, /something/) or a existing subscription)
		// this.mqlManager("/mqlManager/", this.config.mqlQuery, (mql) => {
		// 	if(mql.matches){
		// 		console.log("IS MATCHING");
		// 	} else {
		// 		console.log("IS NOT MATCHING");
		// 	}
		// });

		// Add a Subscription to a existing Mql Manager Subscription
		// this.sub("/mqlManager/", () => console.log("Will be triggered by the mql sub /mqlManager/ as well"));
		// this.sub("/mqlManager/somethingElse/", () => console.log("Will be triggered by the mql sub /mqlManager/ as well"));

		// Remove a Mql Manager Subscription
		// this.mqlManagerRemove("/mqlManager/");

		// UTILITIES
		// ---------------------------------------
		// Element On / Off Events
		// this.on(".selector", "click", () => console.log("test"));
		// this.off(".selector", "click");

		// Element Class Methods
		// this.addClass(element, "myclass");
		// this.addClass(element, "myclass", false); // Disable prefixing of the class name (same for all other class methods)
		// this.removeClass(element, "myclass");
		// this.toggleClass(element, "myclass");
		// this.hasClass(element, "myclass");

		// Element Methods - Append / Prepend
		// (first argument can be a String HTML Tag, Node Element or Dom Element)
		// this.prependTo("div", this.body, { width:"100px" });
		// this.appendTo("div", this.body, { width:"100px" });

		// Element Methods - Element Selector, Elements ForEach, Elements Cloning
		// (elementsSource and elementsTarget can be a String CSS Selector, Node Element or Dom Element)
		// this.elements(elementsSource); // OR this.elements(Node / Dom Element);
		// this.elementsEach(elementsSource, (el) => { console.log(el); }); // OR Node / Dom Element
		// this.elementsClone(elementsSource, elementsTarget); // Clone elementsSource into elementsTarget
		// this.elementsClone(elementsSource, elementsTarget, false); // Same as above, but without renaming the id's and classes

		// General Helpers
		// this.windowWidth();  // Get Window Width
		// this.windowHeight(); // Get Window Height

		// Logging
		// (will be only triggered if this.config.debug is set to true)
		// this.log("title", obj);
		// this.logAll(); // Logs config, events, ui elements

		// ...
	}
}
