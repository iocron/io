class IoNav extends Io {
	constructor(options={}){
		// Super Class Constructor (needed if you use inheritance)
		super(options);

		// Set Defaults, Options & Config
		this.name = "ionav";
		this.defaults = {
			prefix: this.name + "-",
			mqlQuery: {						// Media Query Parameters (use a camelcase css notation)
				minWidth:"0px",
				maxWidth:"1024px"
			},
			// elements: [],				// See io.js (Array (HTML Elements) | NodeList | String)
			elementsNav: [],				// Array (HTML Elements) | NodeList | String (Selector)
			elementsNavCollapsable: true,	// TODO: elementsNavCollapsable still needs to be integrated!!
			buttonTarget: document.body,	// HTML Element | NodeElement
			buttonElement: "div",			// HTML Tag Name | NodeElement
			linkSubmenuClickable: false, // Generates a additional submenu item on submenu's to click
			activeClassToggleOthers: true, // only one active element is possible at the same time
			theme: "default",				// String (Choose a Theme) - NOT IMPLEMENTED YET!
			lang: document.querySelector("html[lang]") ? document.querySelector("html[lang]").lang.slice(0,2) : "en",
			langDict: {
				 "de": "Zur Kategorie ",
				 "en": "To category ",
				 "it": "Alla categoria ",
				 "es": "A la categoría ",
				 "fr": "À la catégorie "
			}
		};

		// Merge Plugin Defaults and User Options to one Config
		// (everything is accessible through this.config then)
		Object.assign(this.config, this.defaults, this.options);

		// Initialize Mobi Main-Logic
		this.sub("/init/", this.init);
		this.pub("/init/");
	}

	init(){
		// UI Elements
		this.ui.btn = this.prependTo(typeof buttonElement === "string" ? "div" : this.config.buttonElement, this.config.buttonTarget);
		this.ui.overlay = this.prependTo("div", this.body, { "class":this.config.prefix+"overlay" });
		this.ui.canvas = this.prependTo("div", this.ui.overlay, { "class":this.config.prefix+"canvas" });

		this.addClass(this.ui.btn, "btn");
		this.addClass(this.html, "-closed");

		// Subscribers
		this.sub("/initComplete/", this.initComplete);
		this.sub("/initOverlay/", this.initOverlay);
		this.sub("/initElementsNav/", this.elementsNav);
		this.sub("/openOverlay/", this.openOverlay);
		this.sub("/closeOverlay/", this.closeOverlay);
		this.sub("/toggleOverlay/", this.toggleOverlay);
		this.sub("/destroyOverlay/", this.destroyOverlay);
		this.sub("/destroyAll/", this.destroyAll);

		this.mqlManager("/mqlMain/", this.config.mqlQuery, (mql) => {
			if(mql.matches){
				// console.log("IS MATCHING");
				this.addClass(this.html, "-active");
			} else {
				// console.log("IS NOT MATCHING");
				this.closeOverlay();
				this.removeClass(this.html, "-active");
			}
		});

		// Publishers & Events
		this.pub("/initOverlay/", this.config.elements);
		this.pub("/initElementsNav/", this.config.elementsNav);
		this.on(this.ui.btn, "click", () => this.pub("/toggleOverlay/"));
		this.on(this.ui.overlay, "click", (e) => {
			if(e.target === e.currentTarget){
				this.pub("/closeOverlay/");
			}
		});

		this.pub("/initComplete/");

		// Logging
		this.logAll();
	}

	initComplete(){
		// Once the Initialization completes...
	}

	destroyAll(){
		this.log("destroyAll()", "Destroying all events and elements");
		for(let prop in this.ui){
			this.ui[prop].remove();
		}
		this.ui = {};
		this.unsub(this.name);
	}

	destroyOverlay(){
		this.log("destroyOverlay()", "Destroying Overlay and Canvas");
		this.unsub("/initOverlay/");
		this.ui.canvas.remove();
		this.ui.overlay.remove();
		delete this.ui.canvas;
		delete this.ui.overlay;
	}

	initOverlay(elements){
		this.log("initOverlay()", "Initializing Overlay");
		this.elementsClone(elements, this.ui.canvas);
	}

	toggleOverlay(){
		this.log("toggleOverlay()", "Toggling Overlay");

		if(this.hasClass(this.html, "-open")){
			this.pub("/closeOverlay/");
		} else {
			this.pub("/openOverlay/");
		}
	}

	openOverlay(){
		this.log("openOverlay()", "Opening Overlay");
		this.addClass(this.html, "-open");
		this.removeClass(this.html, "-closed");
	}

	closeOverlay(){
		this.log("closeOverlay()", "Closing Overlay");
		this.removeClass(this.html, "-open");
		this.addClass(this.html, "-closed");
	}

	elementsNav(selector){
		let activeClassName = this.config.prefix + "-active";

		document.querySelectorAll(selector).forEach((el) => {
			el.querySelectorAll("ul li").forEach((li, index) => {
				let link = li.querySelector(":scope > a");
				let subnav = li.querySelector(":scope > .hasCustomSubnav") ? li.querySelector(":scope > .hasCustomSubnav") : li.querySelector(":scope > ul");

				if(link){
					this.addClass(link, "-hasSubnav");
				}

				if(subnav){
					this.addClass(subnav, "-hasSubnav");

					if(!link){
						// Show Subnavigation if no Toggler / Link is set (otherwise the subnav is not accessible at all)
						this.addClass(subnav, "-active");
					}
				}

				if(link && subnav){
					this.addClass(li, "-hasSubnav");
					this.on(link, "click", (e) => {
						e.preventDefault();

						// If activeClassToggleOthers flag is set, then only one
						// active element (on the same level) is possible at the same time
						if(this.config.activeClassToggleOthers){
							li.parentNode.querySelectorAll(":scope > *").forEach((eli) => {
								if(!eli.isEqualNode(li) && !eli.isEqualNode(link) && !eli.isEqualNode(subnav)){
									eli.querySelectorAll(":scope > *").forEach((eli2) => {
										this.removeClass(eli2, "-active");
									});
								}
							});
						}

						// Toggle active classes
						this.toggleClass(li, "-active");
						this.toggleClass(link, "-active");
						this.toggleClass(subnav, "-active");
					});

					// Add a additional link button (optional, see linkSubmenuClickable), so the user is able to click on the main category,
					// otherwise the user is only able to open the accordion/submenu, not the main category link itself
					if(this.config.linkSubmenuClickable && subnav){
						let linkSubmenuClickable = this.prependTo("li", subnav);
						this.addClass(linkSubmenuClickable, "-linkSubmenuClickable");
						linkSubmenuClickable.innerHTML = link.outerHTML;
						linkSubmenuClickable.querySelector("a").textContent = this.config.langDict[this.config.lang] + linkSubmenuClickable.querySelector("a").textContent;
					}
				}
			});
		});
	}
}
