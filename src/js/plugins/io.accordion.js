class IoAccordion extends Io {
	constructor(options={}){
		// Super Class Constructor (needed if you use inheritance)
		super(options);

		// Set Defaults, Options & Config
		this.name = "ioaccordion";		// Change this to your needs
		this.defaults = {					// All default params are optional, except "prefix"
			prefix: this.name + "-",	// Neccessary (change only if really needed)
			accToggle: ".ioaccordion-toggle",
			accContent: ".ioaccordion-content",
			accContainer: false				// Selector (e.g. .ioaccordion-group)
		};

		// Merge Plugin Defaults and User Options to one Config
		// (everything is accessible through this.config then)
		Object.assign(this.config, this.defaults, this.options);

		// Subscribers
		this.sub("/init/", this.init);
		this.sub("/open/", this.openAccordion);
		this.sub("/close/", this.closeAccordion);

		// Publishers
		this.pub("/init/");
	}

	init(){
		this.ui.accContainer = this.config.accContainer ? this.elements(this.config.accContainer) : [this.body];
		this.ui.accContent = [];
		this.ui.accToggle = [];
		this.ui.accContainer.forEach((accContainer, accContainerCnt) => {
			let accToggle = accContainer.querySelectorAll(this.config.accToggle);
			let accContent = accContainer.querySelectorAll(this.config.accContent);
			let accContentWrapper = [];

			if(this.config.debug){
				this.ui.accContent.push(accContent);
				this.ui.accToggle.push(accToggle);
			}

			if(accToggle.length != accContent.length){
				this.log("init()[WARNING]", "Different amounts of Accordion Togglers Vs. Contents.", "\naccToggles: ", accToggle, "\naccContents: ", accContent);
			}

			accContent.forEach((el) => {
				let accW = this.wrap(el);
				accW.style.maxHeight = 0;
				this.addClass(accW, "content-wrapper");
				accContentWrapper.push(accW);
			});

			// TODO: Needs some refactoring.. (all in pub/sub needed, +more dynamic, accContentWrapper refactoring..)
			accToggle.forEach((el, cnt) => {
				let accContentNext = accContent[cnt].parentNode;
				let accContentNextHeight = this.heightContent(accContentNext);

				this.on(el, "click", (e) => {
					accToggle.forEach((accT) => {
						if(el != accT){
							this.pub("/close/", accT);
						} else {
							this.pub("/open/", accT);
						}
					});
					accContentWrapper.forEach((accCW, accIndex) => { // TODO: Replace/Make compatible to pub/sub
						if(cnt == accIndex){
							this.toggleClass(accCW, "-active");
							accCW.style.maxHeight = this.height(accCW) == 0 ? accContentNextHeight + "px" : 0;
						} else {
							this.removeClass(accCW, "-active");
							accCW.style.maxHeight = 0;
						}
					});
				});
			});
		});

		// Logging
		this.logAll();
	}

	openAccordion(element){
		this.toggleClass(element, "-active");
	}

	closeAccordion(element){
		this.removeClass(element, "-active");
	}
}
