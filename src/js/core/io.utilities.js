// ToDo: Expand all methods functionality by using "this" element, instead of the function parameter "element" for "easier" use
class IoUtilities {
	// https://stackoverflow.com/questions/1350581/how-to-get-an-elements-top-position-relative-to-the-browsers-viewport
	position(element){
		var rect = element.getBoundingClientRect();
		var win = element.ownerDocument.defaultView;
		return { top: rect.top + win.pageYOffset, left: rect.left + win.pageXOffset };
	}

	windowWidth(){
		return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	}

	windowHeight(){
		return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	}

	width(element, width){
		if(typeof width != "undefined"){
			element.style.width = Number.isInteger(width) ? width + "px" : width;
		} else {
			return typeof this.elements(element)[0] != "undefined" ? this.elements(element)[0].clientWidth || 0 : 0;
		}
	}

	height(element, height){
		if(typeof height != "undefined"){
			element.style.height = Number.isInteger(height) ? height + "px" : height;
		} else {
			return typeof this.elements(element)[0] != "undefined" ? this.elements(element)[0].clientHeight || 0 : 0;
		}
	}

	heightContent(element){
		let elStyle = window.getComputedStyle(element);
		if(elStyle.display == "none" || elStyle.height == "0px" || elStyle.maxHeight == "0px"){
			var elStyleInline = {
				display:element.style.display,
				height:element.style.height,
				maxHeight:element.style.maxHeight
			};
			element.style.display = "block";
			element.style.height = "auto";
			element.style.maxHeight = "none";
		}

		let elementHeight = element.clientHeight || 0;

		if(typeof elStyleInline != "undefined"){
			element.style.display = elStyleInline.display || "";
			element.style.height = elStyleInline.height || "";
			element.style.maxHeight = elStyleInline.maxHeight || "";
		}

		return elementHeight;
	}

	prefixClass(className, prefix=this.config.prefix){
		return prefix ? prefix + className : className;
	}

	addClass(element, className, prefix=this.config.prefix){
		element.classList.add(this.prefixClass(className, prefix));
	}

	removeClass(element, className, prefix=this.config.prefix){
		element.classList.remove(this.prefixClass(className, prefix));
	}

	toggleClass(element, className, prefix=this.config.prefix){
		this.hasClass(element, className) ? this.removeClass(element, className) : this.addClass(element, className);
	}

	hasClass(element, className, prefix=this.config.prefix){
		return element.classList.contains(this.prefixClass(className, prefix));
	}

	addChild(elementSrc, elementTarget, attributes, position){
		let elementNode = typeof elementSrc === "string" ? document.createElement(elementSrc) : elementSrc;

		if(attributes){
			for(var attr in attributes){
				elementNode.setAttribute(attr,attributes[attr]);
			}
		}

		this.attr(elementNode, attributes);

		if(position === "prepend" || position === "top"){
			elementTarget.prepend(elementNode);
		} else if(position === "append" || position === "bottom"){
			elementTarget.append(elementNode);
		} else {
			// TODO: Implementing a addChild position default??
		}

		return elementNode;
	}

	attr(element, props, val){
		if(typeof val == "undefined" && typeof props == "object"){
			for(var prop in props){
				element.setAttribute(prop, props[prop]);
			}
		} else if(typeof props == "string" && val){
			element.setAttribute(props, val);
		} else {
			this.log("attr()", "wrong arguments given. Use attr(element, prop, val) or attr(element, { prop: val, ... }) instead.");
		}
	}

	css(element, props, val){
		if(typeof val == "undefined" && typeof props == "object"){
			for(var prop in props){
				element.style[prop] = props[prop];
			}
		} else if(typeof props == "string" && val){
			element.style[props] = val;
		} else {
			this.log("css()", "wrong arguments given. Use css(element, prop, val) or css(element, { prop: val, ... }) instead.");
		}
	}

	wrap(element, wrapper){
		let wrapperElement = typeof wrapper == "string" || typeof wrapper == "undefined" ? document.createElement(wrapper || "div") : wrapper;
		element.parentNode.insertBefore(wrapperElement, element);
		wrapperElement.appendChild(element);
		return wrapperElement;
	}

	prependTo(elementSrc, elementTarget, attributes){
		return this.addChild(elementSrc, elementTarget, attributes || {}, "top");
	}

	appendTo(elementSrc, elementTarget, attributes){
		return this.addChild(elementSrc, elementTarget, attributes || {}, "bottom");
	}

	next(element, selector) {
		var sibling = element.nextElementSibling;
		if (!selector){
			return sibling;
		}
		while(sibling){
			if(sibling.matches(selector)){ return sibling; }
			sibling = sibling.nextElementSibling;
		}
	}

	prev(element, selector){
		var sibling = element.previousElementSibling;
		if(!selector){
			return sibling;
		}
		while(sibling){
			if(sibling.matches(selector)){ return sibling; }
			sibling = sibling.previousElementSibling;
		}
	}

	elements(selector){
		return typeof selector === "string" ? Array.from(document.querySelectorAll(selector)) : (selector.constructor.name == "NodeList" ? Array.from(selector) : [selector]);
	}

	elementsEach(selector, func){
		let elements = this.elements(selector);
		// TODO: There seems to be a scoping bug if there is a elementsEach in a elementsEach loop (check for alternatives)
		// TODO: Alternative??: elements.forEach((fn) => func.call(this, fn));
		elements.forEach(func);
		return elements;
	}

	elementsClone(elementSrc, elementTarget, saveCopy=true){
		return this.elementsEach(elementSrc ? elementSrc : this.config.elements, (el) => {
			let elClone = el.cloneNode(true),
				 elClasslist = Array.from(elClone.classList);

			if(saveCopy){
				elClasslist.forEach((cssClass) => {
					elClone.classList.remove(cssClass);
					elClone.classList.add(this.config.prefix + cssClass);
				});

				if(elClone.id){
					elClone.id = this.config.prefix + elClone.id;
				}
			}

			this.addClass(el, "source");
			this.addClass(elClone, this.config.prefix + "target");
			elementTarget.append(elClone);
		});
	}

	elementsTarget(selector, func){ // TODO: Targets prefixed selectors/id elements (Really Needed?!?!)

	}

	elementsFilter(selector){ // TODO: Needs to be implemented!! (only if needed?!)

	}

	elementBreakpoint(element){
		// TODO: Return a breakpoint on which the element will definitely break / not fit into the available space
		// (needs to check multiple breakpoints dynamically, how?!?!)

	}

	// Add Event to a Element
	on(selector, eventName, func){ // TODO: on/off need an ELEMENT STACK (to remove all elements events later on at once if neccessary?!)
		if(eventName){
			this.elementsEach(selector, (el) => {
				el._events = typeof el._events === "object" ? el._events : {};
				el._events[eventName] = Array.isArray(el._events[eventName]) ? el._events[eventName] : [];
				el._events[eventName].push(func);
				el.addEventListener(eventName, func);
			});
		} else {
			this.error("on()", selector + " - 2nth argument (eventName) is missing.");
		}
	}

	// Remove Event of a Element
	off(selector, eventName, func){
		if(eventName){
			this.elementsEach(selector, (el) => {
				if(func){
					el.removeEventListener(eventName, func);
					el._events[eventName] = el._events[eventName].filter(f => f.toString() != func.toString());
				} else {
					el._events[eventName].forEach(fn => el.removeEventListener(eventName, fn));
					el._events[eventName] = [];
				}
			});
		} else {
			this.error("off()", selector + " - 2nth argument (eventName) is missing.");
		}
	}

	log(title, ...args){
		if(this.config.debug){
			console.log((typeof title === "string" && args.length ? this.name + "." + title + ":" : title), ...args);
		}
	}

	logAll(){
		if(this.config.debug){
			console.groupCollapsed(this.name + " (settings)");
			this.log("config", this.config);
			if(typeof this._eventListObject != "undefined"){
				this.log("events", this._eventListObject("/" + this.name + "/"));
			}
			this.log("ui", this.ui);
			console.groupEnd();
		}
	}

	error(title, ...args){
		if(this.config.debug){
			console.error((typeof title === "string" && args.length ? this.name + "." + title + ":" : title), ...args);
		}
	}
}
