class Io extends IoUtilities {
	constructor(options={}){
		// Super Class Constructor (needed if you use inheritance)
		super();

		// Set Defaults, Options & Config
		this.options = options;
		this.config = {};
		this.ui = {};
		this.body = document.body;
		this.html = document.documentElement;
		this.name = "io";
		this.defaults = {
			prefix: this.name + "-",
			mqlQuery: false,				// Object | With Media Query Parameters (use a camelcase css notation),
			elements: [],					// Array (HTML Elements) | NodeList | String
			debug: false					// true | false (default) - Debug Messages
		};

		// Merge Plugin Defaults and User Options to one Config
		// (everything is accessible through this.config then)
		Object.assign(this.config, this.defaults, this.options);

		if(!window.io_events){
			window.io_events = {};
		}

		// Logging
		// ToDo: if(use logAll only if the class is directly instantiated){...
		// this.logAll();
	}

	// MQL - Public Methods
	mqlManager(path, mql, fn){
		let mqlQuery = mql ? mql : this.config.mqlQuery,
			 mQuery = this._mqlQuery(mqlQuery),
			 mMatch = this._mqlMatch(mQuery);

		mMatch._fn = fn;
		mMatch._path = this._eventPath(path);
		mMatch.addEventListener("change", this._mqlHandler); // Deprecated "addListener" replaced (see: https://stackoverflow.com/questions/68309889/addlistener-is-deprecated-whats-the-alternative#answer-68309940)
		fn.call(this, mMatch);
		this.sub(path, fn, { _mql:mMatch });
	}

	mqlManagerRemove(path){
		if(path){
			io._eventListValuesData(path).forEach((mql) => {
				mql.removeListener(mql._fn);
			});
			this.unsub(path);
		} else {
			this.error("mqlManagerRemove", "No path argument specified or empty");
		}
	}

	// MQL - Private Methods
	_mqlMatch(mqlQuery){
		return window.matchMedia(mqlQuery);
	}

	_mqlQuery(mqlObj){
		let mqlQuery = "";
		for(let m in mqlObj){
			if(mqlObj[m]){
				if(mqlQuery.length != 0){
					mqlQuery += " and ";
				}
				mqlQuery += "(" + m.replace( /([a-z])([A-Z])/g, '$1-$2' ).toLowerCase() + ": "+ mqlObj[m] +")";
			}
		}
		return mqlQuery;
	}

	_mqlHandler(mql){
		if(mql){
			if(this && this._path){
				io.pub(this._path, mql);
			} else if(mql._path){
				io.pub(mql._path, mql);
			} else {
				io.log("_mqlHandler()", "No _path defined");
			}
		} else {
			io.log("_mqlHandler()", "mql argument is undefined");
		}
	}

	// Pub/Sub Events - Public Methods
	pub(eventPath, ...args){ /* TODO: Implementing Async Promises (e.g. async pub(...))?!? */
		// TODO: Optimaly _eventListValues(), etc. should use _eventPath() internally instead of const path!
		setTimeout(() => {
			const path = this._eventPath(eventPath);
			if(typeof window.io_events[path] !== "undefined"){
				this._eventListValuesFunctions(path).forEach((fn) => {
					if(typeof fn == "function"){
						fn.apply(this, args);
					} else if(typeof fn[0] == "function"){ // Weird IE Behavior
						fn[0].apply(this, args);
					} else {
						this.log("pub()", fn + "is not a function.");
					}
				});
			} else {
				this.log("pub()", path + " does not exist (yet).");
			}
		}, 0);
	}

	sub(eventPath, fn, data){
		const path = this._eventPath(eventPath);
		if(typeof fn === "function"){
			window.io_events[path] = typeof window.io_events[path] === "undefined" ? {} : window.io_events[path];
			if(!Array.isArray(window.io_events[path]["_fn"])){
				window.io_events[path]["_fn"] = [];
			}
			if(data){
				if(typeof data === "object"){
					if(typeof window.io_events[path]["_data"] === "object"){
						Object.assign(window.io_events[path]["_data"], data);
					} else {
						window.io_events[path]["_data"] = data;
					}
				} else {
					this.error("sub()", path + " - the data argument has to be of the type object {}");
				}
			}
			window.io_events[path]["_fn"].push(fn);
		} else {
			this.error("sub()", path + " - 2nth argument needs to be a function.");
		}
   }

	unsub(eventPath, fn=false){
		const path = this._eventPath(eventPath);
		if(typeof window.io_events[path] !== "undefined"){
			if(fn && typeof window.io_events[path]["_fn"] !== "undefined"){
				window.io_events[path]["_fn"] = window.io_events[path]["_fn"].filter(f => f.toString() != fn.toString());
			} else {
			  this._eventListKeys(path).forEach((eventP) => {
				  delete window.io_events[eventP];
			  });
			}
		} else {
			this.log("unsub()", path + " does not exist.");
		}
   }

	// Pub/Sub Events - Private Methods
	_eventListKeys(eventPath){
		let result = [];
		for(let prop in window.io_events){
		  if(!eventPath || prop.lastIndexOf(eventPath, 0) === 0){
			  result.push(prop);
		  }
		}
		return result;
	}

	_eventListObject(eventPath){
		if(eventPath){
			let result = {};
			this._eventListKeys(eventPath).forEach((prop) => {
				result[prop] = window.io_events[prop];
			});
			return result;
		}
		return window.io_events;
	}

	_eventListValues(eventPath, eventProp){
		let result = [];
		this._eventListKeys(eventPath).forEach((prop) => {
			// ToDo: Maybe needs a rewrite (because of "..Values" in general),
			// _fn doesn't have to be used here (maybe another method _eventListFunctions?! Or a filter arg instead?!)
			if(!eventPath || typeof window.io_events[prop][eventProp] !== "undefined"){
				if(Array.isArray(window.io_events[prop][eventProp])){
					window.io_events[prop][eventProp].forEach(fn => result.push(fn));
				} else if(typeof window.io_events[prop][eventProp] === "object") {
					for(let propObj in window.io_events[prop][eventProp]){
						result.push(window.io_events[prop][eventProp][propObj]);
					}
				} else {
					result.push(window.io_events[prop][eventProp]);
				}
			}
		});
		return result;
   }

	_eventListValuesFunctions(eventPath){
		return this._eventListValues(eventPath, "_fn");
	}

	_eventListValuesData(eventPath){
		return this._eventListValues(eventPath, "_data");
	}

	_eventPath(eventPath){ // Relative Event Path (with this.name as prefix)
		return (this.name != "io" && eventPath.lastIndexOf("/" + this.name + "/", 0) === -1 ? "/" + this.name : "") + (eventPath.charAt(0) == "/" ? "" : "/") + eventPath + (eventPath.charAt(eventPath.length - 1) == "/" ? "" : "/");
	}
}
var io = new Io();
